﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;

namespace KS.Character
{

    [RequireComponent(typeof(AICharacterControlKSModified))]
    public class SetNMAgentDestination : MonoBehaviour
    {
        private AICharacterControlKSModified aiCharControl;

        [SerializeField] private float surfaceOffset = 0;

        // Start is called before the first frame update
        void Start()
        {
            aiCharControl = GetComponent<AICharacterControlKSModified>();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown((int) MouseButton.LeftMouse))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit) == true)
                {

                    GameObject pickedTarget = new GameObject();
                    pickedTarget.transform.position = hit.point + hit.normal * surfaceOffset;

                    aiCharControl.SetTarget(pickedTarget.transform);

                    Destroy(pickedTarget, 0.2f);
                }
            }
        }
    }
}