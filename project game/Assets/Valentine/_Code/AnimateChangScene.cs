﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AnimateChangScene : MonoBehaviour
{
    public Animator transition;
    public float transsitionTime = 1f;
    public ButtonState StartButton;
    public bool sound;

    void Update()
    {
        if (StartButton._currentState == ButtonState.State.Down)
        {
            LoadNextLevel();
            sound = true;
        }

        if (StartButton._currentState == ButtonState.State.Up && sound ==true)
        {
            sound = false;
        }
        
    }

    public void LoadNextLevel()
    {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
    }

    IEnumerator LoadLevel(int levelIndex)
    {
        transition.SetTrigger("Chang");

        yield return new WaitForSeconds(transsitionTime);

        SceneManager.LoadScene(levelIndex);
    }
}
