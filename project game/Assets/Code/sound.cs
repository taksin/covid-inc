﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sound : MonoBehaviour
{
    public AudioClip lvl;
    AudioSource m_source;
    public static sound instant;
    void Start()
    {
        m_source = GetComponent<AudioSource>();
        instant = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void lvlup()
    {
        m_source.PlayOneShot(lvl, m_source.volume);
    }
}
