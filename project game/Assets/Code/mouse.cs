﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;
using UnityStandardAssets.Characters.ThirdPerson;

[RequireComponent(typeof(AICharacterControl))]
public class mouse : MonoBehaviour
{
    private AICharacterControl aiCharControl;
    [SerializeField]
    private float surfaceOffset = 0;
    void Start()
    {
        aiCharControl = GetComponent<AICharacterControl>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown((int) MouseButton.LeftMouse))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit) == true)
            {
                GameObject pickedTarget = new GameObject();
                pickedTarget.transform.position = hit.point + hit.normal * surfaceOffset;
                aiCharControl.SetTarget(pickedTarget.transform);
                Destroy(pickedTarget, 0.2f);
            }
        }
    }
}
