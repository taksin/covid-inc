﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class panda : MonoBehaviour
{
    public bool ani;
    private Animator _Animator;
    public static panda instant;
    [SerializeField]
    private GameObject car;
    [SerializeField]
    private GameObject Foodpanda;
    public Vector3 pointspawn;
    void Start()
    {
        instant = this;
        _Animator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (ani == true)
        {
            _Animator.SetBool("start",true);
            ani = false;
        }
        else if (ani == false)
        {
            _Animator.SetBool("start",false);
        }

        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag =="Fire")
        {
            Instantiate(Foodpanda, pointspawn, Quaternion.identity);
            Debug.Log("trig");
        }
        
    }
}
