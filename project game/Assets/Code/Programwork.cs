﻿using System;
using UnityEngine.UI;
using UnityEngine;

public class Programwork : MonoBehaviour
{
    public float percent =0;
    [SerializeField]
    Text programText;
    [SerializeField]
    Slider programslider;
    public GameObject lightwork;
    public int upgradeprogram=1;
    public static Programwork instant;
    bool lightworking;
    public GameObject winbar;
    public bool hiregame;
   
    public void Start()
    {
        instant = this;
    }
    void Update()
    {
        programText.text = "Program : " + Mathf.Round(percent).ToString();
        programslider.value = percent;
        if (lightworking == true)
        {
            lightwork.SetActive(true);
        }
        if (lightworking == false)
        {
            lightwork.SetActive(false);
        }
        if (percent >= 100)
        {
            bool isActive = winbar.activeSelf;
            winbar.SetActive(true);
        }
        if (hiregame == true)
        {
            percent += Time.deltaTime / 10 * upgradeprogram * Feeling.fill.foodnum;
            sum.instant.programperday += Time.deltaTime /10 * upgradeprogram * Feeling.fill.foodnum;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Computer")
        {
            nextday.instant.moneysum += Time.deltaTime ;
            Debug.Log(1);
            lightworking = true;
        }
    }
}
