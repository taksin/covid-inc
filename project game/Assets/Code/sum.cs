﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sum : MonoBehaviour
{
    public Text day;
    public Text art;
    public Text program;
    public Text money;
    
    public int daysum;
    public bool Reset;
    public float programperday;
    public float artperday;
    public static sum instant;
    [SerializeField]
    public GameObject sumtab;
    void Start()
    {
        instant = this;
        sumtab.SetActive(false);
    }

    
    void Update()
    {
        money.text = "" + Mathf.Round(nextday.instant.moneysum*100);
        day.text = "" + maintime.instant.daytime;
        program.text = "" + Mathf.Round(programperday);
        art.text = "" + Mathf.Round(artperday);
    }
}
