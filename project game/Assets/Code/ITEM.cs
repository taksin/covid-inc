﻿using UnityEngine;

public class ITEM : MonoBehaviour
{
    public GameObject Itembar;
    public bool active;
    public static ITEM instant;
    public void Start()
    {
        bool isActive = Itembar.activeSelf;
        Itembar.SetActive(!isActive);
        instant = this;
    }
    public void OpenPanel()
    {
        if (Itembar != null)
        {
            bool isActive = Itembar.activeSelf;
            Itembar.SetActive(!isActive);
             
        }
    }
}
