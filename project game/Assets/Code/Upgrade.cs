﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrade : MonoBehaviour
{
    public static Upgrade instant;
    private void Start()
    {
        instant = this;
    }
    public void upgradecom() 
    {
        money.pay.moneyhave -= 5000;
        Programwork.instant.upgradeprogram = 2;
        Upgrademanage.instant.com = 2;
        sound.instant.lvlup();
    }
    public void upgradecomver2()
    {
        money.pay.moneyhave -= 15000;
        Programwork.instant.upgradeprogram = 3;
        Upgrademanage.instant.com = 3;
        sound.instant.lvlup();
    }
    public void upgradeart()
    {
        money.pay.moneyhave -= 5000;
        ART.instant.upgradeart = 2;
        Upgrademanage.instant.art = 2;
        sound.instant.lvlup();
    }
    public void upgradeartver2()
    {
        money.pay.moneyhave -= 15000;
        ART.instant.upgradeart = 3;
        Upgrademanage.instant.art = 3;
        sound.instant.lvlup();
    }
}
