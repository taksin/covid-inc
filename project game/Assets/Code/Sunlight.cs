﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sunlight : MonoBehaviour
{
    [SerializeField] private float sunRotationDegreePerSecond = 30.0f;
    private Light sun;
    public bool next;
    public static Sunlight instant;
    void Start()
    {
        sun = this.GetComponent<Light>();
        instant = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (next == true)
        {
            transform.rotation = Quaternion.Euler(33, -30, -4);
            next = false;
        }

        sun.transform.Rotate(new Vector3(0, 1, 0), this.sunRotationDegreePerSecond * Time.deltaTime);
    }
}
