﻿using System;
using UnityEngine;

public class upgradegel : MonoBehaviour
{

    public GameObject gel1;
     public GameObject gel2;
     public GameObject gel3;
     public GameObject button1;
     public GameObject button2;
     

     public int gelint=0;
     public static upgradegel instant;
     public void Start()
     {
         instant = this;
         gelint = 0;
         gel1.SetActive(true);
     }

     public void upgel2()
     {
         gel1.SetActive(false);
         gel2.SetActive(true);
         gelint = 1;
     }
     public void upgel3()
     {
         gel1.SetActive(false);
         gel2.SetActive(false);
         gel3.SetActive(true);
         gelint = 2;
     }
    
    
    
}
