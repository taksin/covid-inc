﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class nextday : MonoBehaviour
{
    
    public float moneysum;
    public static nextday instant;
    public ButtonState click;
    
    void Start()
    {
        instant = this;
    }
    void Update()
    {   
        
        if (click._currentState == ButtonState.State.Down)
        {
            Time.timeScale = 1;
            sum.instant.programperday = 0;
            sum.instant.artperday = 0;
            Debug.Log("reset");
            sum.instant.sumtab.SetActive(false);
            money.pay.addmoney = true;
            click._currentState =ButtonState.State.Up;
            Sunlight.instant.next = true;
        }
        else if(click._currentState == ButtonState.State.Up)
        {
            
            Time.timeScale = 0;
            Debug.Log(moneysum);
            sum.instant.sumtab.SetActive(true);
        }
    }
    
}
