﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Feeling : MonoBehaviour
{
    public float gel ;
    [SerializeField]
    public Slider statusbar;
    [SerializeField]
    public Slider feelbar;
    [SerializeField]
    public Slider foodbar;
    [SerializeField]
    Text statustext;
    [SerializeField]
    Text feeltext;
    [SerializeField]
    Text foodtext;
    public GameObject lossbar;
    [SerializeField]
    public float foodnum=1;
    public static Feeling fill;
    public float prop=0;
    void Start()
    {
        fill = this ;
        statustext.text = "";
    }
        void Update()
        {
            feelbar.value += prop/300;
        foodbar.value -= Time.deltaTime/100;
        if(foodbar.value <= 0.3)
        {
            foodtext.text="Hungry";
            foodnum = 0.5f;
            feelbar.value -= Time.deltaTime/100;
        }
        if(foodbar.value >= 0.4)
        {
            foodtext.text="Normal";
            foodnum = 1;
        }
        if (feelbar.value >= 0.31 && feelbar.value <= 0.79)
        {
            feeltext.text = "Normal";
        }
        if (feelbar.value <= 0.3)
        {
            feeltext.text = "Stress";
        }
        if (feelbar.value >= 0.8)
        {
            feeltext.text = "Happy";
        }

        if (statusbar.value <= 0)
        {
            feelbar.value -= Time.deltaTime/100;
        }
    }
        public void OnTriggerStay(Collider collisionInfo)
    {   if (Gelfill.gelout.gelrefill >= 1)
        {
            if (collisionInfo.gameObject.tag == "Gel")
            {
                statustext.text = "Washing";
            }
            if (collisionInfo.gameObject.tag == "Gel" && gel <= 5 && upgradegel.instant.gelint == 0 || upgradegel.instant.gelint == 1)
            {
                gel+=Time.deltaTime;
                
            }
            if (collisionInfo.gameObject.tag == "Gel" && gel <= 5 && upgradegel.instant.gelint == 2)
            {
                gel *= 2;
                gel+=Time.deltaTime;
                
            }

            if (collisionInfo.gameObject.tag == "Food" && statusbar.value <= 0)
            {
                feelbar.value -= 1;
            }
        }
        if (collisionInfo.gameObject.tag == "Computer")
        {
            statustext.text = "Working";
            gel-=Time.deltaTime*maintime.instant.covid/5;
        }
        if (collisionInfo.gameObject.tag == "Artitem")
        {
            statustext.text = "Working";
            gel -= Time.deltaTime;
        }
        statusbar.value = gel;
        
        if(gel<=0)
            gel=0;
    }

        private void OnTriggerExit(Collider collisionInfo)
        {
            if (statusbar.value >= 2.1 )
            {
                statustext.text = "";
            }
            if (statusbar.value <= 2 )
            {
                statustext.text = "Dirty";
            }
        }
}
