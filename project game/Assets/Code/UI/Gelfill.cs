﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gelfill : MonoBehaviour
{
    public int gelrefill = 100;
    [SerializeField]
    Text showempty;
    public static Gelfill gelout;
    void Start()
    {
        gelout = this;
        if (upgradegel.instant.gelint == 0)
        {
            gelrefill = 300;
        }
        if (upgradegel.instant.gelint == 1)
        {
            gelrefill = 500;
        }
    }


    void Update()
    {

        if (gelrefill == 0)
        { showempty.text = "Empty"; }
        else if (gelrefill >= 1)
        {
            showempty.text = "" + gelrefill;
        }

        
    }
    public void OnTriggerStay(Collider collisionusegel)
    {
        if (gelrefill >= 1)
        {
            if (collisionusegel.gameObject.tag == "Staff" && Feeling.fill.gel <= 5)
            {
                gelrefill-=1;
            }
        }
    }
}

