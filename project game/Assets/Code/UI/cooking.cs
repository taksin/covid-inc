﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cooking : MonoBehaviour
{
    public Slider cook;
    public Vector3 point;
    public GameObject food;
    public GameObject slider;
    public GameObject light;
    private bool wave;
    void Start()
    {
        
    }

    void Update()
    {
        if (cook.value >= 10)
        {
            cook.value = 0;
            Instantiate(food, point, Quaternion.identity);
            wave = false;
            light.SetActive(false);
            slider.SetActive(false);
        }

        if (wave == true)
        {
            cook.value += Time.deltaTime;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "cooking")
        {
            wave = true;
            light.SetActive(true);
            slider.SetActive(true);
        }
    }
}
