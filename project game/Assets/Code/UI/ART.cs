﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ART : MonoBehaviour
{
    [SerializeField]
    float Artvalue = 0;
    [SerializeField]
    Text ArtText;
    [SerializeField]
    Slider Artlider;
    public int upgradeart=1;
    public static ART instant;
    public bool hireart;
    // Update is called once per frame
    private void Start()
    {
        instant = this;
    }

    void Update()
    {
        ArtText.text = "Art : " + Mathf.Round(Artvalue).ToString(); ;
        Artlider.value = Artvalue;
        if (hireart == true)
        {
            Artvalue += Time.deltaTime / 10 * upgradeart * Feeling.fill.foodnum;
            sum.instant.artperday += Time.deltaTime /10 * upgradeart * Feeling.fill.foodnum;
        }
    }
    public void OnTriggerStay(Collider collisionInfo)
    {
        if (collisionInfo.gameObject.tag == "Artitem")
        {
            Artvalue += Time.deltaTime  /10* upgradeart * Feeling.fill.foodnum;
            sum.instant.artperday += Time.deltaTime /10 * upgradeart * Feeling.fill.foodnum;
        }
    }
}
