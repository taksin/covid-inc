﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class maintime : MonoBehaviour
{
    float minute = 0;
    public float hour = 8;
    public int daytime = 1;
    public static maintime instant;
    [SerializeField]
    Text showtime;
    [SerializeField]
    Text showday;
    public int covid;
    public Text covidtext;
    public GameObject sum;
    [SerializeField]
    private Slider covidbar;
    void Start()
    {
        showtime.text = minute.ToString();
        instant = this;
    }
    void Update()
    {
        if (minute >= 9  )
        {
            hour += 1;
            minute = 0;
        }
        if (hour >= 17.59 && hour <= 18)
        {
            sum.SetActive(true);
        }
        if (hour >= 18)
        {
            hour = 8;
            daytime += 1;
        }
        covid = daytime * 2;
        minute += Time.deltaTime;
        showtime.text ="TIME : "+ hour.ToString()+":"  + Mathf.Round(minute).ToString();
        showday.text = "Day : " + daytime;
        covidtext.text = "COVID : " + covid;
        covidbar.value = covid;
    }
}
