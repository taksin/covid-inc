﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panelserect : MonoBehaviour
{
    public GameObject foodserect;
    public GameObject gel;
    public GameObject upgrade;
    public GameObject hire;
    
    void Start()
    {
       
    }
    void Update()
    { 
        
    }
    
    
    public void Openfood() 
    {
        
        if (foodserect != null)
        {
            bool isActive = foodserect.activeSelf;
            foodserect.SetActive(!isActive);
            gel.SetActive(false);
            upgrade.SetActive(false);
            hire.SetActive(false);
            Upgrademanage.instant.UI1.SetActive(true);
            Upgrademanage.instant.UI2.SetActive(false);
            Upgrademanage.instant.UI3.SetActive(false);
            Upgrademanage.instant.UI4.SetActive(false);
        }
    }
    

    public void Opengel()
    {
        
        if (gel != null )
        {
            bool isActive = gel.activeSelf;
            gel.SetActive(!isActive);
            foodserect.SetActive(false);
            upgrade.SetActive(false);
            hire.SetActive(false);
            Upgrademanage.instant.UI1.SetActive(false);
            Upgrademanage.instant.UI2.SetActive(true);
            Upgrademanage.instant.UI3.SetActive(false);
            Upgrademanage.instant.UI4.SetActive(false);
        }
    }
    public void Openupgrade()
    {
        
        if (upgrade != null )
        {
            bool isActive = upgrade.activeSelf;
            upgrade.SetActive(!isActive);
            gel.SetActive(false);
            foodserect.SetActive(false);
            hire.SetActive(false);
            Upgrademanage.instant.UI1.SetActive(false);
            Upgrademanage.instant.UI2.SetActive(false);
            Upgrademanage.instant.UI3.SetActive(true);
            Upgrademanage.instant.UI4.SetActive(false);
        }
    }
    public void Openhire()
    {
        
        if (hire != null )
        {
            bool isActive = hire.activeSelf;
            hire.SetActive(!isActive);
            gel.SetActive(false);
            upgrade.SetActive(false);
            foodserect.SetActive(false);
            Upgrademanage.instant.UI1.SetActive(false);
            Upgrademanage.instant.UI2.SetActive(false);
            Upgrademanage.instant.UI3.SetActive(false);
            Upgrademanage.instant.UI4.SetActive(true);
        }
    }
}
