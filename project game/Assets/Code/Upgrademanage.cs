﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrademanage : MonoBehaviour
{
    public GameObject Gamep1v1;
    public GameObject Gamep2v1;
    public GameObject Gamep1v2;
    public GameObject Gamep2v2;
    public GameObject Gamep1v3;
    public GameObject Gamep2v3;
    public GameObject Artp2v1;
    public GameObject Artp2v2;
    public GameObject Artp2v3;
    public GameObject buttoncom;
    public GameObject buttonart;
    public GameObject buttoncom2;
    public GameObject buttonart2;
    public GameObject UI1;
    public GameObject UI2;
    public GameObject UI3;
    public GameObject UI4;
    public int com;
    public int art;
    public static Upgrademanage instant;
    void Start()
    {
        instant = this;
        com = 1;
    }
    void Update()
    {
        if (com == 2)
        {
            Gamep1v1.SetActive(false);
            Gamep2v1.SetActive(false);
            Gamep1v2.SetActive(true);
            Gamep2v2.SetActive(true);
            buttoncom.SetActive(false);
        }
        if (com == 3)
        {
            Gamep1v1.SetActive(false);
            Gamep2v1.SetActive(false);
            Gamep1v2.SetActive(false);
            Gamep2v2.SetActive(false);
            Gamep1v3.SetActive(true);
            Gamep2v3.SetActive(true);
            buttoncom.SetActive(false);
            buttoncom2.SetActive(false);
        }
        if (art == 2)
        {
            Artp2v1.SetActive(false);
            Artp2v2.SetActive(true);
            buttonart.SetActive(false);
        }
        if (art == 3)
        {
            Artp2v1.SetActive(false);
            Artp2v2.SetActive(false);
            Artp2v3.SetActive(true);
            buttonart.SetActive(false);
            buttonart2.SetActive(false);
        }
    }
}
